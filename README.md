# esp32-ulp-test

A test to see how fast the ESP32 ULP can toggle GPIOs.  Note that this
doesn't do anything useful; it just serially outputs a bit pattern on
a GPIO as a timing test.

