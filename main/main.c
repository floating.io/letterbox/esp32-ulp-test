#include "driver/rtc_io.h"
#include "esp_sleep.h"
#include "soc/rtc_periph.h"
#include "ulp_riscv.h"
#include "ulp_main.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "display.h"

// Config
#define DISPLAY_CLOCK (GPIO_NUM_10)
#define DISPLAY_DATA  (GPIO_NUM_11)
#define DISPLAY_LATCH (GPIO_NUM_12)

volatile display_buf_t *display_buffer = (display_buf_t *)&ulp_linebuf;

extern const uint8_t ulp_main_bin_start[] asm("_binary_ulp_main_bin_start");
extern const uint8_t ulp_main_bin_end[]   asm("_binary_ulp_main_bin_end");

void init_ulp_program() {
  esp_err_t err = ulp_riscv_load_binary(ulp_main_bin_start, (ulp_main_bin_end - ulp_main_bin_start));
  ESP_ERROR_CHECK(err);

  /* The first argument is the period index, which is not used by the ULP-RISC-V timer
   * The second argument is the period in microseconds, which gives a wakeup time period of: 20ms
   */
  ulp_set_wakeup_period(0, 16000);
  
  /* Start the program */
  err = ulp_riscv_run();
  ESP_ERROR_CHECK(err);
}



void app_main(void) {
  printf("Starting...\n");

  switch (esp_sleep_get_wakeup_cause()) {
  case ESP_SLEEP_WAKEUP_ULP:
        printf("ULP-RISC-V woke up the main CPU! \n");
        break;
        
  case ESP_SLEEP_WAKEUP_EXT0:
    printf("wake: RTC-IO\n");
    break;

  case ESP_SLEEP_WAKEUP_EXT1:
    printf("wake: RTC-CNTL\n");
    break;

  case ESP_SLEEP_WAKEUP_TIMER:
    printf("wake: TIMER\n");
    break;

  case ESP_SLEEP_WAKEUP_TOUCHPAD:
    printf("wake: TOUCHPAD\n");
    break;

  case ESP_SLEEP_WAKEUP_GPIO:
    printf("wake: GPIO\n");
    break;
    
  case ESP_SLEEP_WAKEUP_UART:
    printf("wake: UART\n");
    break;
    
  case ESP_SLEEP_WAKEUP_WIFI:
    printf("wake: WIFI\n");
    break;
    
  case ESP_SLEEP_WAKEUP_BT:
    printf("wake: BT\n");
    break;
    
  case ESP_SLEEP_WAKEUP_COCPU:
    printf("wake: COCPU\n");
    break;
    
  case ESP_SLEEP_WAKEUP_COCPU_TRAP_TRIG:
    printf("wake: COCPU_TRAP_TRIG\n");
    break;

  default:
        printf("Not a ULP-RISC-V wakeup, initializing it! \n");
        init_ulp_program();
  }
  
  for (int i = 0; i < 10; i ++) {
    printf("%d...\n", i);
    vTaskDelay(pdMS_TO_TICKS(1000));
  }
  
  /* Go back to sleep, only the ULP Risc-V will run */
  printf("Entering in deep sleep\n\n");
  
  /* Small delay to ensure the messages are printed */
  vTaskDelay(100);
  
  //  ESP_ERROR_CHECK( esp_sleep_enable_ulp_wakeup() );
  esp_deep_sleep_start();
}
