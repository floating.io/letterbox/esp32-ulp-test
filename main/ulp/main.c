//------------------------------------------------------------------------------
// Simple test for ULP GPIO toggle speed with respect to our 48-bit stream.
//------------------------------------------------------------------------------

#include <stdint.h>
#include "ulp_riscv.h"
#include "ulp_riscv_utils.h"
#include "ulp_riscv_gpio.h"
#include "display.h"

// Config
#define DISPLAY_CLOCK (GPIO_NUM_10)
#define DISPLAY_DATA  (GPIO_NUM_11)
#define DISPLAY_LATCH (GPIO_NUM_12)

// First dimension is scan lines; second dimension is bit pattern.
volatile display_buf_t linebuf = {
  { 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA },
  { 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA },
  { 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA },
  { 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA },
  { 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA },
  { 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA },
  { 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA },
};

int main(void) {
   ulp_riscv_gpio_init(DISPLAY_DATA);
   ulp_riscv_gpio_input_disable(DISPLAY_DATA);
   ulp_riscv_gpio_output_enable(DISPLAY_DATA);
   ulp_riscv_gpio_set_output_mode(DISPLAY_DATA, RTCIO_MODE_OUTPUT);
   ulp_riscv_gpio_pullup_disable(DISPLAY_DATA);
   ulp_riscv_gpio_pulldown_disable(DISPLAY_DATA);
  
   ulp_riscv_gpio_init(DISPLAY_CLOCK);
   ulp_riscv_gpio_input_disable(DISPLAY_CLOCK);
   ulp_riscv_gpio_output_enable(DISPLAY_CLOCK);
   ulp_riscv_gpio_set_output_mode(DISPLAY_CLOCK, RTCIO_MODE_OUTPUT);
   ulp_riscv_gpio_pullup_disable(DISPLAY_CLOCK);
   ulp_riscv_gpio_pulldown_disable(DISPLAY_CLOCK);
  
   ulp_riscv_gpio_init(DISPLAY_LATCH);
   ulp_riscv_gpio_input_disable(DISPLAY_LATCH);
   ulp_riscv_gpio_output_enable(DISPLAY_LATCH);
   ulp_riscv_gpio_set_output_mode(DISPLAY_LATCH, RTCIO_MODE_OUTPUT);
   ulp_riscv_gpio_pullup_disable(DISPLAY_LATCH);
   ulp_riscv_gpio_pulldown_disable(DISPLAY_LATCH);
 
   for (int scanline = 0; scanline < 7; scanline++) {
     for (int byte = 0; byte < 6; byte++) {
       for (int bit = 0; bit < 8; bit++) {
	 ulp_riscv_gpio_output_level(DISPLAY_DATA , (linebuf[scanline][byte] & (0x01 << bit)) > 0 ? 1 : 0);
	 ulp_riscv_gpio_output_level(DISPLAY_CLOCK, 1);
	 ulp_riscv_gpio_output_level(DISPLAY_CLOCK, 0);
       }
     }
     ulp_riscv_gpio_output_level(DISPLAY_LATCH, 1);
     ulp_riscv_gpio_output_level(DISPLAY_LATCH, 0);
   }

   ulp_riscv_gpio_output_level(DISPLAY_DATA, 0);
}
